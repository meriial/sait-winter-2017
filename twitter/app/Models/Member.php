<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model {

    public $timestamps = false;

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getTweets()
    {
        return Tweet::where('member_id', $this->id)->get();
    }
    
    public function tweetsIWrote()
    {
        return $this->hasMany(Tweet::class);
    }
    
    public function tweetsILike()
    {
        return $this->belongsToMany(Tweet::class, 'likes');
    }
    
    public function getTweetsILike()
    {
        $result = \DB::table('likes')
            ->select('tweets.*')
            ->join('tweets', 'tweets.id', '=', 'likes.tweet_id')
            ->where('likes.member_id', $this->id)
            ->get();
        
        return $result;
    }

}









