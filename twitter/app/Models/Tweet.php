<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Tweet extends Model {

    public $timestamps = false;
    
    public function getUser()
    {
        return User::find($this->user_id);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function getUsersWhoLiked()
    {
        // $sql = "SELECT members.* FROM likes JOIN members ON members.id = likes.member_id WHERE tweet_id = x";
        
        $result = \DB::table('likes')
            ->select('users.*')
            ->join('users', 'users.id', '=', 'likes.user_id')
            ->where('likes.tweet_id', $this->id)
            ->get();
        
        return $result;
    }
    
    public function usersWhoLikedMe()
    {
        return $this->belongsToMany(User::class, 'likes');
    }
}








