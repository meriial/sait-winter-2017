<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Tweet;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    public function fullName()
    {
        return $this->name;
    }

    public function getTweets()
    {
        return Tweet::where('user_id', $this->id)->get();
    }
    
    public function tweetsIWrote()
    {
        return $this->hasMany(Tweet::class);
    }
    
    public function tweetsILike()
    {
        return $this->belongsToMany(Tweet::class, 'likes');
    }
    
    public function getTweetsILike()
    {
        $result = \DB::table('likes')
            ->select('tweets.*')
            ->join('tweets', 'tweets.id', '=', 'likes.tweet_id')
            ->where('likes.user_id', $this->id)
            ->get();
        
        return $result;
    }
    
}
