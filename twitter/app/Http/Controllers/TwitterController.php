<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tweet;
use App\User;

class TwitterController extends Controller
{
    public function home()
    {
        $data = [
            'tweets' => Tweet::all(),
            'users' => User::all()
        ];

        return view('twitter', $data);
    }
    
    public function user()
    {
        $user = $this->getUserFromUrl();
        
        if (!$user) {
            die('That user does not exist.');
        }

        $data = [
            'user' => $user,
            'tweets' => $user->getTweets(),
        ];

        return view('user', $data);
    }
    
    public function postUser()
    {
        // if (empty($_POST['content']) || empty($_POST['description'])) {
        //     die('You must enter a tweet and a description.');
        // }
        
        $this->validate(request(), [
            'content' => [
                'required',
                'regex:/#.*/'
            ],
        ], [
            'regex' => 'You must enter a hashtag.',
        ]);
        
        
        // $user = $this->getUserFromUrl();
        $user = request()->user();
        
        $tweet = new Tweet;
        $tweet->content = $_POST['content'];
        $tweet->user_id = $user->id;
        $tweet->number_of_replies = 0;
        $tweet->date = date('Y-m-d H:i:s');
        $tweet->save();
        
        return redirect('user?id='.$user->id);
    }
    
    public function getUserFromUrl()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $user = User::find($id);
        } else if (isset($_GET['handle'])) {
            $handle = $_GET['handle'];
            $user = User::where('handle', $handle)->first();
        } else {
            die('You must enter an id or handle in the URL.');
        }
        
        return $user;
    }
}
