<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Tweet;
use App\Models\Member;

Route::get('/', 'TwitterController@home');

Route::get('/user', 'TwitterController@user');

Route::post('/user', 'TwitterController@postUser');



Auth::routes();

Route::get('/home', 'HomeController@index');
