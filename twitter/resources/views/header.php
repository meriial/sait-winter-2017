<?php if(Auth::check()) { ?>
    <div class="">
        Hello there, <?php echo request()->user()->name ?>
        <form action="logout" method="post">
            <?php echo csrf_field() ?>
            <button type="submit" name="button">logout</button>
        </form>
        <!-- <a href="logout">logout</a> -->
    </div>
<?php } else { ?>
    <a href="login">login</a>
<?php } ?>