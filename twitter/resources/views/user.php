<?php include 'header.php' ?>

<h1><?php echo $user->fullName() ?>: Tweets</h1>

<?php if($errors) { ?>
    
    <ul class="error" style="background:red; color:white;">
        <?php foreach ($errors->all() as $message) { ?>
            <li><?php echo $message ?></li>
        <?php } ?>
    </ul>
    
<?php } ?>

<?php if(Auth::check()) { ?>
    <form action="" method="post">
        
        <?php echo csrf_field(); ?>
        
        <input type="text" name="content" value="<?php echo old('content') ?>" />
        
        <input type="submit" name="submit" value="Submit" />
        
    </form>
<?php } ?>

<ul>
    <?php foreach ($user->tweetsIWrote as $tweet) { ?>
        <li>
            <?php echo $tweet->content ?> 
            (<?php echo count($tweet->usersWhoLikedMe) ?>)
            <br>
            <?php echo $tweet->user->fullName() ?>
        </li>
    <?php } ?>
</ul>

<ul>
    <?php foreach ($user->tweetsILike as $tweet) { ?>
        <li>
            <?php echo $tweet->content ?> <br>
            <?php echo $tweet->user->fullName() ?>
        </li>
    <?php } ?>
</ul>
