<?php include 'header.php' ?>

<h1>Tweets</h1>

<ul>
    <?php foreach ($tweets as $tweet) { ?>
        <li>
            <?php echo $tweet->content ?> (<?php echo count($tweet->getUsersWhoLiked()) ?>)
            <?php foreach ($tweet->getUsersWhoLiked() as $user): ?>
                <?php echo $user->name ?>
            <?php endforeach; ?>
            <br>
            <?php echo $tweet->getUser()->fullName() ?>
        </li>
    <?php } ?>
</ul>
<?php
