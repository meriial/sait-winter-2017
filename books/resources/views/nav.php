<nav>
    <ul>
        <li <?php if($page == 'books') {echo 'class="active"';} ?>>
            <a href="books">Books</a>
        </li>
        <li <?php if($page == 'contact') {echo 'class="active"';} ?>>
            <a href="contact">Contact</a>
        </li>
        <li <?php if($page == 'about') {echo 'class="active"';} ?>>
            <a href="about">About</a>
        </li>
    </ul>
