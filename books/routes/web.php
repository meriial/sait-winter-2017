<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/books', function () {
    $data = [
        'page' => 'books'
    ];
    return view('books', $data);
});

Route::get('/contact', function () {
    $data = [
        'page' => 'contact'
    ];
    return view('contact', $data);
});

Route::get('/about', function () {
    $data = [
        'page' => 'about'
    ];
    return view('about', $data);
});
