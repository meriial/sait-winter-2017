<pre>

<?php

$foo = [
  'eyes' => 'blue',
  'hair' => 'brown'
];

$foo['eyes'] = 'brown';
$foo['age'] = 22;

var_dump(isset($foo['eyes']));

foreach ($foo as $trait => $description) {
  var_dump($trait, $description);
}

echo $foo['eyes']."\n";

echo $foo['whatever'];

// $total = count($array);
//
// for ($i=0; $i < $total; $i++) {
//   var_dump($array[$i]);
// }

var_dump($foo);
