<pre>

<?php

class Person {
  public $eyes;

  public function __construct($eyes)
  {
    $this->eyes = $eyes;
  }

  public function greet()
  {
    return "Hello!! My eyes are $this->eyes\n";
  }
}

$bob = new Person('brown');

$jane = new Person('blue');

echo $bob->greet();
echo $jane->greet();




// var_dump($bob, $jane);
