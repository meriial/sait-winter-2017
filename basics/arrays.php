<h1>Hello World!</h1>

<?php
echo '<pre>';
$array = [
  'bob',
  'jane',
  'phil'
];

$array[] = 2;
$array[] = 'haha';

foreach ($array as $value) {
  var_dump("$value");
}
// $total = count($array);
//
// for ($i=0; $i < $total; $i++) {
//   if (isset($array[$i])) {
//     var_dump($array[$i]);
//   } else {
//     echo "$i is empty\n";
//   }
// }

var_dump($array);
