<pre>

<?php

function add($a, $b)
{
  return $a + $b;
}

$result = add(add(1, 2), add(3, 4));
$result = 1+2+3+4;

// echo add(5,6);

// var_dump($result);

$crowd = ['Bob', 'Jane', 'Roger'];

foreach ($crowd as $person) {
  echo greet($person);
}

$foo = 'bar';

function greet($name)
{
  echo $foo;
  return 'Hi '.$name.', how are you doing on '.date('Y-m-d')."\n";
}
