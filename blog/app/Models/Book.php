<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {
  // public $title;
  public $contents = 'Once upon a time';

  // public function __construct($title)
  // {
  //   $this->title = $title;
  // }

  public function read()
  {
      return $this->contents;
  }

  public function getMember()
  {
      return Member::find($this->member_id);
  }
}
