<?php

class Attributes {
  public $eyes;
  public $hair;

  public function __construct($eyes, $hair)
  {
      $this->eyes = $eyes;
      $this->hair = $hair;
  }

  public function comments()
  {
      $comments = [
          'This is awesome',
          'This sucks',
          'This is better that the other thing.'
      ];

      shuffle($comments);

      return $comments;
  }

  public function colour()
  {
    $colours = [
      'red',
      'blue'
    ];

    shuffle($colours);

    return $colours[0];
  }
}
