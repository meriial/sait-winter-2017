<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\ComicBook;
use App\Models\Book;
use App\Models\Member;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/me', function() {
  $data = [
    'numbers' => [1,2,4],
    'people' => [
      'John' => [
        'eyes' => 'blue',
        'hair' => 'blond'
      ],
      'Jane' => [
        'eyes' => 'green',
        'hair' => 'red'
      ],
      'Marge' => [
        'eyes' => 'black',
        'hair' => 'blue'
      ]
    ]
  ];

  return view('me', $data);
});

Route::get('/me2', function() {
  $data = [
    'people' => [
      'John' => new Attributes('blue', 'blonde'),
      'Jane' => new Attributes('green', 'red'),
      'Marge' => new Attributes('black', 'blue'),
    ]
  ];

  return view('me2', $data);
});

Route::get('/books', function() {
  $data = [
    'books' => Book::all(),
    'friends' => Member::all(),
    'page' => 'books'
  ];

  return view('books', $data);
});


Route::get('/about', function() {
  $data = [
      'page' => 'about'
  ];

  return view('about', $data);
});
