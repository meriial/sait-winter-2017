<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body>
    <?php include 'header.php'; ?>

    <main class="container">
        <?php include 'nav.php' ?>

        <h2>About Us</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </main>
    <footer>
        Copyright <?php echo date('Y') ?>
    </footer>
</body>
</html>
