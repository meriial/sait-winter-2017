<style>
  .blue {
    background: blue;
  }
  .red {
    background: red;
  }
</style>

<h1>Hello, Cameron</h1>

<ul>
  <?php foreach ($people as $name => $attributes) { ?>
    <li class="<?= $attributes->colour() ?>">
      <?= "$name has {$attributes->eyes} eyes and {$attributes->hair} hair." ?>
      <ol>
        <?php foreach ($attributes->comments() as $comment) { ?>
          <li><?= $comment ?></li>
        <?php } ?>
      </ol>
    </li>
  <?php } ?>
</ul>
