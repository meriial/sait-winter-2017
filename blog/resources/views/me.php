<h1>Hello, Cameron</h1>

<ul>
  <?php foreach ($people as $name => $attributes) { ?>
    <li><?php echo "$name has {$attributes['eyes']} eyes and {$attributes['hair']} hair." ?></li>
    <li><?php echo $name . ' has ' . $attributes['eyes'] . ' eyes and '.$attributes['hair'] . 'hair.' ?></li>
  <?php } ?>
</ul>

// John has blue eyes and blond hair
// Jane has green eyes and red hair
// Marge has black eyes and blue hair
