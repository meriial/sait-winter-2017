<!DOCTYPE html>
<html>

<?php include('head.php') ?>

<body>
    <?php include 'header.php'; ?>

    <main class="container">

        <?php include 'nav.php' ?>

        <div class="col-md-6">
            <h2>Books</h2>
            <?php foreach($books as $book) { ?>
                <div class="book">
                    <h3><?php echo $book->title ?></h3>
                    <p><?php echo $book->read() ?></p>
                </div>
                <?php } ?>
            </div>
            <div class="col-md-6">
                <h2>Friends</h2>
                <?php foreach ($friends as $friend) { ?>
                    <h3>
                        <?php echo $friend->last_name ?>,
                        <?php echo $friend->first_name ?> -
                        <?php echo $friend->email ?>
                    </h3>
                <?php } ?>
            </div>
        </main>
        <footer>
            Copyright <?php echo date('Y') ?>
        </footer>
    </body>
</html>
